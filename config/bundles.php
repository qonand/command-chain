<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Symfony\Bundle\MonologBundle\MonologBundle::class => ['all' => true],
    App\CommandChainBundle\CommandChainBundle::class => ['all' => true],
    App\FooBundle\FooBundle::class => ['all' => true],
    App\BarBundle\BarBundle::class => ['all' => true],
    App\BazBundle\BazBundle::class => ['all' => true],
];
