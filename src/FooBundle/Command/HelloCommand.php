<?php 

namespace App\FooBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class HelloCommand.
 *
 * Command to display "Hello from Foo!" phrase
 */
class HelloCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected static $defaultName = 'foo:hello';

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Hello from Foo!');

        return Command::SUCCESS;
    }
}

