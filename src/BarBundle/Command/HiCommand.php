<?php

namespace App\BarBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class HiCommand.
 *
 * Command to display "Hi from Bar!" phrase
 */
class HiCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected static $defaultName = 'bar:hi';

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Hi from Bar!');

        return Command::SUCCESS;
    }
}

