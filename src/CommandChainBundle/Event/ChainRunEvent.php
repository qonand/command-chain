<?php

namespace App\CommandChainBundle\Event;

/**
 * Class ChainRunEvent.
 *
 * The event that will be published when a chain will run
 */
class ChainRunEvent extends AbstractChainEvent
{
    /**
     * @var string
     */
    public const NAME = 'command_chain.chain_run';

    /**
     * @var string[] list of command names which are members of the chain
     */
    private $members;

    /**
     * ChainRunEvent constructor.
     *
     * @param string $master name of a master command
     * @param string[] $members list of command names which are members of the chain
     */
    public function __construct(string $master, array $members)
    {
        parent::__construct($master);
        $this->members = $members;
    }

    /**
     * Get list of command names which are members of the chain
     *
     * @return string[] list of command names
     */
    public function getMembers(): array
    {
        return $this->members;
    }
}
