<?php

namespace App\CommandChainBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class AbstractChainCommandEvent.
 *
 * Base class for all events of chain commands
 */
abstract class AbstractChainCommandEvent extends Event
{
    /**
     * @var string a command name that was run
     */
    private $commandName;

    /**
     * @var bool status to show if a command is master
     */
    private $isMaster;

    /**
     * ChainCommandRunEvent constructor.
     *
     * @param string $commandName a command name that was run
     * @param bool $isMaster status to show if a command is master
     */
    public function __construct(string $commandName, bool $isMaster)
    {
        $this->commandName = $commandName;
        $this->isMaster = $isMaster;
    }


    /**
     * Get a command name that was run
     *
     * @return string the command name
     */
    public function getCommandName(): string
    {
        return $this->commandName;
    }

    /**
     * Get master status of a command
     *
     * @return bool master status
     */
    public function isMaster(): bool
    {
        return $this->isMaster;
    }
}
