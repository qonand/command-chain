<?php

namespace App\CommandChainBundle\Event;

/**
 * Class ChainCommandTerminatedEvent.
 *
 * The event that will be published when a chain command will be terminated
 */
class ChainCommandTerminatedEvent extends AbstractChainCommandEvent
{
    /**
     * @var string the event name
     */
    public const NAME = 'command_chain.chain_command_terminated';

    /**
     * @var string result of a command output
     */
    private $output;

    /**
     * ChainCommandTerminatedEvent constructor.
     *
     * @param string $commandName a command name that was terminated
     * @param bool $isMaster a master status of a command
     * @param string $output result of a command output
     */
    public function __construct(string $commandName, bool $isMaster, string $output)
    {
        parent::__construct($commandName, $isMaster);
        $this->output = $output;
    }

    /**
     * Get result of a command output
     *
     * @return string command output
     */
    public function getOutput(): string
    {
        return $this->output;
    }
}
