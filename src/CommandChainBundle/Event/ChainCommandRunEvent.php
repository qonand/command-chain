<?php

namespace App\CommandChainBundle\Event;

/**
 * Class ChainCommandRunEvent.
 *
 * The event that will be published when a chain command will run
 */
class ChainCommandRunEvent extends AbstractChainCommandEvent
{
    /**
     * @var string the event name
     */
    public const NAME = 'command_chain.chain_command_run';
}
