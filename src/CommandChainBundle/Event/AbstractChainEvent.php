<?php

namespace App\CommandChainBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class AbstractChainEvent.
 *
 * Base class for all events of chains
 */
abstract class AbstractChainEvent extends Event
{
    /**
     * @var string name of a master command
     */
    private $master;

    /**
     * ChainTerminatedEvent constructor.
     *
     * @param string $master name of a master command
     */
    public function __construct(string $master)
    {
        $this->master = $master;
    }

    /**
     * Get name of a master command
     *
     * @return string master command name
     */
    public function getMaster(): string
    {
        return $this->master;
    }
}
