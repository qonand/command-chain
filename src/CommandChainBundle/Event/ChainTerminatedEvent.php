<?php

namespace App\CommandChainBundle\Event;

/**
 * Class ChainTerminatedEvent.
 *
 * The event that will be published when a chain will be terminated
 */
class ChainTerminatedEvent extends AbstractChainEvent
{
    /**
     * @var string
     */
    public const NAME = 'command_chain.chain_terminated';
}
