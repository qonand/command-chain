<?php

namespace App\CommandChainBundle\DependencyInjection;

use App\CommandChainBundle\Collection\ChainCollection;
use App\CommandChainBundle\Collection\ChainCollectionInterface;
use App\CommandChainBundle\CommandLoader;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class DecorateCommandLoaderPass.
 *
 * The pass to replace CommandLoader
 */
class DecorateCommandLoaderPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container): void
    {
        $chains = $container->getParameter('command_chains.chains');
        $definition = new Definition(ChainCollection::class, [$chains]);
        $container->setDefinition(ChainCollectionInterface::class, $definition);
        $chainCollection = new Reference(ChainCollectionInterface::class);

        $container->getDefinition('console.command_loader')
            ->setClass(CommandLoader::class)
            ->addMethodCall('setChainCollection',  [$chainCollection]);
    }
}
