<?php

namespace App\CommandChainBundle\Tests;

use App\CommandChainBundle\Collection\ChainCollection;
use App\CommandChainBundle\Collection\Exception\ChainCyclicDependenciesDetectedException;
use App\CommandChainBundle\Collection\Exception\ChainNotFoundException;
use PHPUnit\Framework\TestCase;

/**
 * Class CollectionTest.
 *
 * Class to test chain collection
 */
class CollectionTest extends TestCase
{
    /**
     * @var ChainCollection collection that contains chains
     */
    private $collection;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->collection = new ChainCollection([
            'foo:hello' => [
                'bar:hi'
            ],
        ]);
    }
    /**
     * Test isInChain() method
     */
    public function testIsInChain(): void
    {
        static::assertTrue($this->collection->isInChain('foo:hello'));
        static::assertTrue($this->collection->isInChain('bar:hi'));
        static::assertFalse($this->collection->isInChain('bar:good-evening'));
    }

    /**
     * Test getMembers() method
     */
    public function testGetMembers(): void
    {
        static::assertEquals(['bar:hi'], $this->collection->getMembers('foo:hello'));
        static::assertEquals([], $this->collection->getMembers('bar:hi'));
    }

    /**
     * Test getMembers() method for cases when command doesn't exist
     */
    public function testGetMembersByNonexistentCommand(): void
    {
        $this->expectException(ChainNotFoundException::class);
        $this->collection->getMembers('bar:good-evening');
    }

    /**
     * Test getMembers() method for cases when chains has cyclic dependencies
     */
    public function testGetMembersWithCyclicDependencies(): void
    {
        $collection = new ChainCollection([
            'foo:hello' => [
                'bar:hi'
            ],
            'bar:hi' => [
                'foo:hello'
            ],
        ]);

        $this->expectException(ChainCyclicDependenciesDetectedException::class);
        $collection->getMembers('foo:hello');
    }

    /**
     * Test getMaster() method
     */
    public function testGetMaster(): void
    {
        static::assertEquals('foo:hello', $this->collection->getMaster('bar:hi'));
        static::assertNull($this->collection->getMaster('foo:hello'));
    }
}
