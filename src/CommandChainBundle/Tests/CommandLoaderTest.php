<?php

namespace App\CommandChainBundle\Tests;

use App\CommandChainBundle\Collection\ChainCollection;
use App\CommandChainBundle\Command\CommandDecorator;
use App\CommandChainBundle\CommandLoader;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Command\Command;

/**
 * Class CommandLoaderTest.
 *
 * Class to test command loader
 */
class CommandLoaderTest extends KernelTestCase
{
    /**
     * @var CommandLoader a command loader instance
     */
    private $loader;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $cmd1 = $this->createStub(Command::class);
        $cmd1->method('getName')->willReturn('foo:hello');

        $cmd2 = $this->createStub(Command::class);
        $cmd2->method('getName')->willReturn('bar:hi');

        $application->add($cmd1);
        $application->add($cmd2);

        $this->loader = $kernel->getContainer()->get('console.command_loader');
        $this->loader->setChainCollection(new ChainCollection([
            'foo:hello' => [
                'bar:hi'
            ]
        ]));
    }

    /**
     * Test getting of a master command
     */
    public function testGetMaster(): void
    {
        /** @var CommandDecorator $command */
        $command = $this->loader->get('foo:hello');
        static::assertInstanceOf(CommandDecorator::class, $command);
        static::assertEquals('foo:hello', $command->getName());
        static::assertEquals(['bar:hi'], $command->getMembers());
        static::assertNull($command->getMaster());
        static::assertTrue($command->isMaster());
    }

    /**
     * Test getting of a member command
     */
    public function testGetMember(): void
    {
        /** @var CommandDecorator $command */
        $command = $this->loader->get('bar:hi');
        static::assertInstanceOf(CommandDecorator::class, $command);
        static::assertEquals('bar:hi', $command->getName());
        static::assertEmpty($command->getMembers());
        static::assertEquals('foo:hello', $command->getMaster());
        static::assertFalse($command->isMaster());
    }
}
