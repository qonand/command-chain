<?php

namespace App\CommandChainBundle;

use App\CommandChainBundle\Collection\ChainCollectionInterface;
use App\CommandChainBundle\Command\CommandDecorator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\CommandLoader\ContainerCommandLoader;

/**
 * Class CommandLoader.
 *
 * The command loader to configure chain commands
 */
class CommandLoader extends ContainerCommandLoader
{
    /**
     * @var ChainCollectionInterface collection that contains chains
     */
    private $collection;

    /**
     * Set collection that contains chains
     *
     * @param ChainCollectionInterface $collection the collection that contains chains
     */
    public function setChainCollection(ChainCollectionInterface $collection): void
    {
        $this->collection = $collection;
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $name): Command
    {
        if (!$this->collection->isInChain($name)) {
            return parent::get($name);
        }

        return $this->buildCommandDecorator($name);
    }

    /**
     * Build command decorator by defined command name
     *
     * @param string $name the command name
     *
     * @return CommandDecorator
     */
    private function buildCommandDecorator(string $name): CommandDecorator
    {
        $decorator = clone parent::get('command:decorator');
        $decorator->setName($name);
        $decorator->setBaseCommand(parent::get($name));
        $members = $this->collection->getMembers($name);
        $master = $this->collection->getMaster($name);
        $decorator->setMembers($members);
        $decorator->setMaster($master);

        return $decorator;
    }
}
