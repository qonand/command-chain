<?php

namespace App\CommandChainBundle;

use App\CommandChainBundle\Event\ChainCommandRunEvent;
use App\CommandChainBundle\Event\ChainCommandTerminatedEvent;
use App\CommandChainBundle\Event\ChainRunEvent;
use App\CommandChainBundle\Event\ChainTerminatedEvent;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use App\CommandChainBundle\DependencyInjection\DecorateCommandLoaderPass;

/**
 * Class CommandChainBundle.
 *
 * The bundle to implement chain commands
 */
class CommandChainBundle extends Bundle
{
    /**
     * {@inheritDoc}
     */
    public function build(ContainerBuilder $container): void
    {
        parent::build($container);
        $container->addCompilerPass(new DecorateCommandLoaderPass(), PassConfig::TYPE_REMOVE);

        $container->register('chain_listener', ChainListener::class)
            ->addTag('kernel.event_listener', [
                'event' => ChainRunEvent::class,
                'method' => 'onChainRun',
            ])
            ->addTag('kernel.event_listener', [
                'event' => ChainTerminatedEvent::class,
                'method' => 'onChainTerminated',
            ])
            ->addTag('kernel.event_listener', [
                'event' => ChainCommandRunEvent::class,
                'method' => 'onChainCommandRun',
            ])
            ->addTag('kernel.event_listener', [
                'event' => ChainCommandTerminatedEvent::class,
                'method' => 'onChainCommandTerminated',
            ])
            ->addTag('kernel.event_listener', [
                'event' => ConsoleCommandEvent::class,
                'method' => 'onConsoleCommand',
            ])
            ->addArgument(new Reference(LoggerInterface::class));
    }
}
