<?php

namespace App\CommandChainBundle;

use App\CommandChainBundle\Command\CommandDecorator;
use App\CommandChainBundle\Event\ChainCommandRunEvent;
use App\CommandChainBundle\Event\ChainCommandTerminatedEvent;
use App\CommandChainBundle\Event\ChainRunEvent;
use App\CommandChainBundle\Event\ChainTerminatedEvent;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Event\ConsoleCommandEvent;

/**
 * Class ChainListener.
 *
 * The listener to process events of chains
 */
class ChainListener
{
    /**
     * @var LoggerInterface the logger
     */
    private $logger;

    /**
     * ChainListener constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Process an event when chain is run
     *
     * @param ChainRunEvent $event an event instance
     */
    public function onChainRun(ChainRunEvent $event): void
    {
        $this->logger->info("{$event->getMaster()} is a master command of a command chain that has registered member commands");
        foreach ($event->getMembers() as $member) {
            $this->logger->info("$member registered as a member of {$event->getMaster()} command chain");
        }
    }

    /**
     * Process an event when chain is terminated
     *
     * @param ChainTerminatedEvent $event an event instance
     */
    public function onChainTerminated(ChainTerminatedEvent $event): void
    {
        $this->logger->info("Execution of {$event->getMaster()} chain completed.");
    }

    /**
     * Process an event when chain command is run
     *
     * @param ChainCommandRunEvent $event an event instance
     */
    public function onChainCommandRun(ChainCommandRunEvent $event): void
    {
        if ($event->isMaster()) {
            $this->logger->info("Executing {$event->getCommandName()} command itself first:");
        }
    }

    /**
     * Process an event when chain command is terminated
     *
     * @param ChainCommandTerminatedEvent $event an event instance
     */
    public function onChainCommandTerminated(ChainCommandTerminatedEvent $event): void
    {
        $this->logger->info($event->getOutput());
        if ($event->isMaster()) {
            $this->logger->info("Executing {$event->getCommandName()} chain members:");
        }
    }

    /**
     * Process an event when console command is run
     *
     * @param ConsoleCommandEvent $event an event instance
     */
    public function onConsoleCommand(ConsoleCommandEvent $event): void
    {
        $command = $event->getCommand();
        if (!($command instanceof CommandDecorator)) {
            return;
        }

        $masterCommand = $command->getMaster();
        if ($masterCommand) {
            $event->getOutput()->writeln("Error: {$command->getName()} command is a member of {$command->getMaster()} command chain and cannot be executed on its own");
            $event->disableCommand();
        }
    }
}
