<?php

namespace App\CommandChainBundle\Collection;

use App\CommandChainBundle\Collection\Exception\ChainCyclicDependenciesDetectedException;
use App\CommandChainBundle\Collection\Exception\ChainNotFoundException;

/**
 * Class ChainCollection.
 */
class ChainCollection implements ChainCollectionInterface
{
    /**
     * @var array list of chains
     */
    private $chains;

    /**
     * ChainCollection constructor.
     *
     * @param array $chains list of chains
     */
    public function __construct(array $chains)
    {
        $this->chains = $chains;
    }

    /**
     * {@inheritdoc}
     */
    public function getMembers(string $command): array
    {
        if (!$this->isInChain($command)) {
            throw new ChainNotFoundException("Chain with master command $command not found");
        }

        if (!$this->checkChainDependencies($command)) {
            throw new ChainCyclicDependenciesDetectedException("Chain with master command $command has cyclic dependencies");
        }

        return $this->findMembers($command);
    }

    /**
     * {@inheritdoc}
     */
    public function isInChain(string $command): bool
    {
        return $this->hasMasterCommand($command) || $this->hasMemberCommand($command);
    }

    /**
     * {@inheritdoc}
     */
    public function getMaster(string $command): ?string
    {
        foreach ($this->chains as $master => $members) {
            if (in_array($command, $members, true)) {
                return $master;
            }
        }

        return null;
    }

    /**
     * Check if the collection has a master command with defined name
     *
     * @param string $command the command name to check
     *
     * @return bool result of the checking
     */
    private function hasMasterCommand(string $command): bool
    {
        return array_key_exists($command, $this->chains);
    }

    /**
     * Check if the collection has a member command with defined name
     *
     * @param string $command the command name to check
     *
     * @return bool result of the checking
     */
    private function hasMemberCommand(string $command): bool
    {
        return !empty($this->getMaster($command));
    }

    /**
     * Check if the collection has a chains with cyclic dependencies
     *
     * @param string $command the command name to check
     * @param string[] $stack list of command to execute
     *
     * @return bool result of the checking
     */
    private function checkChainDependencies(string $command, array $stack = []): bool
    {
        $stack[] = $command;
        foreach ($this->findMembers($command) as $member) {
            if (in_array($member, $stack, true)) {
                return false;
            }

            if (!$this->checkChainDependencies($member, $stack)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Find members of a command with defined name
     *
     * @param string $command the command name to find
     *
     * @return array list of command members
     */
    private function findMembers(string $command): array
    {
        return $this->hasMasterCommand($command) ? $this->chains[$command] : [];
    }
}
