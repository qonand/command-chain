<?php

namespace App\CommandChainBundle\Collection;

use App\CommandChainBundle\Collection\Exception\ChainCyclicDependenciesDetectedException;
use App\CommandChainBundle\Collection\Exception\ChainNotFoundException;

/**
 * Interface ChainCollectionInterface.
 */
interface ChainCollectionInterface
{
    /**
     * Check if the collection has a command with defined name
     *
     * @param string $command the command name to check
     *
     * @return bool result of the checking
     */
    public function isInChain(string $command): bool;

    /**
     * Get members of a command with defined name
     *
     * @param string $command the command name
     *
     * @throws ChainCyclicDependenciesDetectedException
     * @throws ChainNotFoundException
     *
     * @return array list of command members
     */
    public function getMembers(string $command): array;

    /**
     * Get a master command name of a command with defined name
     *
     * @param string $command the command name
     *
     * @return string|null the master command name or null if the command
     * doesn't have a master command
     */
    public function getMaster(string $command): ?string;
}