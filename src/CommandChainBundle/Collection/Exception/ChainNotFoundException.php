<?php

namespace App\CommandChainBundle\Collection\Exception;

use LogicException;

/**
 * Class ChainNotFoundException.
 */
class ChainNotFoundException extends LogicException
{
}
