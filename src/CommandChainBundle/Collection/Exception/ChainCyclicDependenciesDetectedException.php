<?php

namespace App\CommandChainBundle\Collection\Exception;

use LogicException;

/**
 * Class ChainCyclicDependenciesDetectedException.
 */
class ChainCyclicDependenciesDetectedException extends LogicException
{
}
