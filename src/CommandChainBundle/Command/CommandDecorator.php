<?php

namespace App\CommandChainBundle\Command;

use App\CommandChainBundle\Event\ChainCommandRunEvent;
use App\CommandChainBundle\Event\ChainCommandTerminatedEvent;
use App\CommandChainBundle\Event\ChainRunEvent;
use App\CommandChainBundle\Event\ChainTerminatedEvent;
use Exception;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\CommandNotFoundException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CommandDecorator.
 *
 * Decorator for commands to implement chain mechanism
 */
class CommandDecorator extends Command
{
    /**
     * {@inheritdoc}
     */
    protected static $defaultName = 'command:decorator';

    /**
     * @var Command a command that will be decorated
     */
    private $baseCommand;

    /**
     * @var EventDispatcherInterface a dispatcher to public chain events
     */
    private $dispatcher;

    /**
     * @var string[] list of member command names for the current command
     */
    private $members = [];

    /**
     * @var string|null a master command name for the current command
     */
    private $master;

    /**
     * AbstractCommandDecorator constructor.
     *
     * @param string|null $name a name of the current command
     * @param EventDispatcherInterface $dispatcher a dispatcher to public chain events
     */
    public function __construct(string $name = null, EventDispatcherInterface $dispatcher)
    {
        parent::__construct($name);
        $this->dispatcher = $dispatcher;
    }

    /**
     * Set a command that will be decorated
     *
     * @param Command $command the command to decorate
     */
    public function setBaseCommand(Command $command): void
    {
        $this->baseCommand = $command;
    }

    /**
     * Set list of member command names for the current command
     *
     * @param string[] $members list of member command names
     */
    public function setMembers(array $members): void
    {
        $this->members = $members;
    }

    /**
     * Get list of member command names for the current command
     *
     * @return string[] list of member command names
     */
    public function getMembers(): array
    {
        return $this->members;
    }

    /**
     * Set a master command name for the current command
     *
     * @param string|null $value a master command name or null if the command doesn't
     * have a master command
     */
    public function setMaster(?string $value): void
    {
        $this->master = $value;
    }

    /**
     * Get a master command name for the current command
     *
     * @return string|null a master command name or null if the command doesn't
     * have a master command
     */
    public function getMaster(): ?string
    {
        return $this->master;
    }

    /**
     * Check if the command is the master command
     *
     * @return bool result of checking
     */
    public function isMaster(): bool
    {
        return !empty($this->members);
    }

    /**
     * Build an instance of a member command by defined name
     *
     * @param string $name the member command name
     *
     * @return Command the instance of command
     */
    private function buildMemberCommand(string $name): Command
    {
        try {
            return $this->getApplication()->find($name);
        } catch (CommandNotFoundException $exception) {
            throw new CommandNotFoundException("{$exception->getMessage()}. Chain is stopped!");
        }
    }

    /**
     * {@inheritdoc}
     *
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if ($this->isMaster()) {
            $event = new ChainRunEvent($this->getName(), $this->members);
            $this->dispatcher->dispatch($event);
        }

        $event = new ChainCommandRunEvent($this->getName(), $this->isMaster());
        $this->dispatcher->dispatch($event);

        $buffer = new BufferedOutput();
        $code = $this->baseCommand->execute($input, $buffer);
        $content = $buffer->fetch();
        $output->write($content);

        $event = new ChainCommandTerminatedEvent($this->getName(), $this->isMaster(), $content);
        $this->dispatcher->dispatch($event);

        if ($code === static::SUCCESS) {
            foreach ($this->members as $member) {
                $command = $this->buildMemberCommand($member);
                $code = $command->run($input, $output);
                if ($code !== static::SUCCESS) {
                    break;
                }
            }
        }

        if ($this->isMaster()) {
            $event = new ChainTerminatedEvent($this->getName());
            $this->dispatcher->dispatch($event);
        }
        return $code;
    }
}
