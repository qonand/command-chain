<?php

namespace App\BazBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class EveningCommand.
 *
 * Command to display "Evening from Baz!" phrase
 */
class EveningCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected static $defaultName = 'baz:evening';

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Evening from Baz!');

        return Command::SUCCESS;
    }
}

